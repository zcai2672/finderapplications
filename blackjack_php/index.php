<!DOCTYPE html>
<html>
	<head>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<script type="text/javascript" >

			$(document).ready(function() {
                $("#submit_btn").click(function(){
    				$.ajax({
    					type : "POST",
    					url : "process.php",
    					data : {
    						card1 : $('#card1').val(),
    						card2 : $('#card2').val()
    					}
    				}).done(function(msg) {
    					var json = JSON.parse(msg);
    					if("error" in json){
    					   $("#msg").html('Error: ' + json.error);
    					   $("#msg").removeClass('valid').addClass('invalid');
    					}
    					
    					if("answer" in json){
    					   $("#msg").html('Total Points: ' + json.answer);
    					   $("#msg").removeClass('invalid').addClass('valid');
    					}
    				});
				});
			});

		</script>

		<style type="text/css">
			.valid {
				color: green;
			}
			.invalid {
				color: red;
			}
		</style>
		<title>Black Jack!</title>
	</head>
	<body>
		<form>
			Card 1
			<input type="text" name='card1' class='card' id='card1' maxlength="2" size=10 />
			<br/>
			Card 2
			<input type="text" name='card2' class='card' id='card2' maxlength="2" size=10 />
			<br/>
			<button type="button" id='submit_btn'>
				Get Points!
			</button>
			<h2 id="msg"></h2>
		</form>
	</body>
</html>