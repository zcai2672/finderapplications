<?php

$card1 = $_POST['card1'];
$card2 = $_POST['card2'];

$faceArray = array("A" => 11, "2" => 2, "3" => 3, "4" => 4, "5" => 5, "6" => 6, "7" => 7, "8" => 8, "9" => 9, "10" => 10, "J" => 10, "Q" => 10, "K" => 10);
$suitArray = array("S", "C", "D", "H");

$json = array();

if (validateCard($card1) && validateCard($card2)) {
    if (!hasDuplicate($card1, $card2)) {
        $json['answer'] = (string)calcPoints($card1, $card2);
    } else {
        $json['error'] = "Duplicate cards";
    }
} else {

    if (!validateCard($card1) && validateCard($card2)) {
        $json['error'] = "Invalid card 1";
    } else if (!validateCard($card2) && validateCard($card1)) {
        $json['error'] = "Invalid card 2";
    } else
        $json['error'] = "Invalid card 1 and card 2";

}
echo json_encode($json);

// Validate individual cards
function validateCard($cardVal) {

    $cardVal = strtoupper($cardVal);

    if (strlen($cardVal) != 2) {
        return false;
    }

    $faceVal = $cardVal{0};
    $suitVal = $cardVal{1};

    if (!array_key_exists($faceVal, $GLOBALS['faceArray']) || !in_array($suitVal, $GLOBALS['suitArray'])) {
        return false;

    }
    return true;
}

// This function Checks for duplicate cards
function hasDuplicate($card1, $card2) {

    if (strtoupper($card1) != strtoupper($card2)) {
        return false;
    }
    return true;
}

// calculate the points for the cards
function calcPoints($card1, $card2) {

    $face1Val = $card1[0];
    $face2Val = $card2[0];

    $face1Val = strtoupper($face1Val);
    $face2Val = strtoupper($face2Val);

    $card1Val = $GLOBALS['faceArray'][$face1Val];
    $card2Val = $GLOBALS['faceArray'][$face2Val];

    if ($face1Val == $face2Val && $face2Val == "A") {// If there is two Aces, one of the ace will become 1
        $card2Val = 1;
    }
    return $card1Val + $card2Val;
}
?>
